package com.selenium.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Mobile {
    public static void main(String[] args) {
        //class reference me dikkat aati haii
        //if mobile me vodaphone ka sim use karna then??? , everything will need to be changed.
//        Airtel airtel = new Airtel() ;
//        airtel.data();
//        airtel.calling();

        //interface better way haii..par best nahiii
//        Sim sim = new Airtel() ;
//        sim.data();
//        sim.calling();


        //spring framework will make objects for us
        //and will manage those objects
        //do touch source code and make source configurabl
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml") ;
        //and the config file in src only as we told spring to searcg config file in src folder only.

        System.out.println("config loaded");

//        Airtel air = (Airtel) context.getBean("airtel") ;
//        air.data();
//        air.calling();

        Sim sim = context.getBean("sim" , Sim.class) ;
        //interface reference variable banaya gaya hai taaki bakchodi naa ho
        sim.data();
        sim.calling();
    }
}
